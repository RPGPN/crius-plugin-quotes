package crius_plugin_quotes

// We'll use realm instead of guild / channel here because we support both GuildCrius *and* LiveCrius
const tableCreateScript = `CREATE TABLE IF NOT EXISTS quotes__quotes
(
    quote_id   serial primary key,
    realm_id  text not null,
    platform  text not null,
    quote text not null
);`
